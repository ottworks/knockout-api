import httpStatus from 'http-status';
import jwt from 'jsonwebtoken';
import request from 'supertest';
import { APP_PORT, JWT_SECRET } from '../config/server';
import knex from '../src/services/knex';
import Rule from '../src/v2/schemas/rules/rule';
import config from '../config/config';
import CreateRuleRequest from '../src/v2/schemas/rules/createRuleRequest';
import UpdateRuleRequest from '../src/v2/schemas/rules/updateRuleRequest';

describe('/v2/rules endpoint', () => {
  const { host } = config[process.env.NODE_ENV];
  const TEST_USER_ID = 990;

  const knockoutJwt = jwt.sign({ id: TEST_USER_ID }, JWT_SECRET, {
    algorithm: 'HS256',
    expiresIn: '2 weeks',
  });

  const api = request(`http://${host}:${APP_PORT}/v2`);

  describe('GET /', () => {
    test('shows all site wide rules when no rule type or id', async () => {
      const rules = await knex('Rules').whereNull('rulable_type').andWhere('rulable_id', null);

      const expectedResponse: Array<Rule> = rules.map((rule) => ({
        id: rule.id,
        rulableType: null,
        rulableId: null,
        category: rule.category,
        title: rule.title,
        cardinality: rule.cardinality,
        description: rule.description,
        createdAt: rule.created_at.toISOString(),
        updatedAt: rule.updated_at.toISOString(),
      }));

      await api.get('/rules').expect(httpStatus.OK, expectedResponse);
    });

    test('shows all rules for a given rule type and rule id', async () => {
      const rules = await knex('Rules').where('rulable_type', 'Subforum').andWhere('rulable_id', 5);

      const expectedResponse: Array<Rule> = rules.map((rule) => ({
        id: rule.id,
        rulableType: 'Subforum',
        rulableId: 5,
        category: rule.category,
        title: rule.title,
        cardinality: rule.cardinality,
        description: rule.description,
        createdAt: rule.created_at.toISOString(),
        updatedAt: rule.updated_at.toISOString(),
      }));

      await api
        .get('/rules?rulableType=Subforum&rulableId=5')
        .expect(httpStatus.OK, expectedResponse);
    });
  });

  describe('POST /', () => {
    beforeAll(async () => {
      await knex('Users').insert({
        id: TEST_USER_ID,
        username: 'Test Mod User',
        avatar_url: 'none.webp',
        usergroup: 3,
        created_at: new Date(2020, 1, 1),
        updated_at: new Date(),
      });
    });

    afterAll(async () => {
      await knex.raw('SET FOREIGN_KEY_CHECKS=0');
      await knex('Users').where('id', TEST_USER_ID).delete();
      await knex.raw('SET FOREIGN_KEY_CHECKS=1');
    });

    test('create a general rule', async () => {
      const data: CreateRuleRequest = {
        category: 'Site Wide Rules',
        title: 'Test Rule',
        cardinality: 1,
        description: 'Test rule with description',
      };

      const expectedResponse: Rule = {
        id: null,
        rulableType: null,
        rulableId: null,
        category: data.category,
        title: data.title,
        cardinality: data.cardinality,
        description: data.description,
        createdAt: null,
        updatedAt: null,
      };

      await api
        .post('/rules')
        .send(data)
        .set('Cookie', `knockoutJwt=${knockoutJwt}`)
        .expect((res) => {
          expectedResponse.id = res.body.id;
          expectedResponse.createdAt = res.body.createdAt;
          expectedResponse.updatedAt = res.body.updatedAt;
        })
        .expect(httpStatus.CREATED, expectedResponse);
    });

    test('rejects an non-moderator user', async () => {
      await knex('Users')
        .update({
          usergroup: 1,
        })
        .where('id', TEST_USER_ID);

      const data: CreateRuleRequest = {
        category: 'Site Wide Rules',
        title: 'Test Rule',
        cardinality: 1,
        description: 'Test rule',
      };

      await api
        .post('/rules')
        .send(data)
        .set('Cookie', `knockoutJwt=${knockoutJwt}`)
        .expect(httpStatus.FORBIDDEN);

      await knex('Users')
        .update({
          usergroup: 5,
        })
        .where('id', TEST_USER_ID);
    });

    test('rejects a logged out user', async () => {
      const data: CreateRuleRequest = {
        category: 'Site Wide Rules',
        title: 'Test Rule',
        cardinality: 1,
        description: 'Test rule',
      };

      await api.post('/rules').send(data).expect(httpStatus.UNAUTHORIZED);
    });
  });

  describe('PUT /:id', () => {
    beforeAll(async () => {
      await knex('Users').insert({
        id: TEST_USER_ID,
        username: 'Test Mod User',
        avatar_url: 'none.webp',
        usergroup: 5,
        created_at: new Date(2020, 1, 1),
        updated_at: new Date(),
      });
    });

    afterAll(async () => {
      await knex.raw('SET FOREIGN_KEY_CHECKS=0');
      await knex('Users').where('id', TEST_USER_ID).delete();
      await knex.raw('SET FOREIGN_KEY_CHECKS=1');
    });

    test('updates a rule', async () => {
      const rule = await knex('Rules').first();
      const data: UpdateRuleRequest = {
        description: 'New Description',
      };

      const expectedResponse: Rule = {
        id: rule.id,
        rulableType: rule.rulable_type,
        rulableId: rule.rulable_id,
        category: rule.category,
        title: rule.title,
        cardinality: rule.cardinality,
        description: data.description,
        createdAt: rule.created_at.toISOString(),
        updatedAt: null,
      };

      await api
        .put(`/rules/${rule.id}`)
        .send(data)
        .set('Cookie', `knockoutJwt=${knockoutJwt}`)
        .expect((res) => {
          expectedResponse.updatedAt = res.body.updatedAt;
        })
        .expect(httpStatus.CREATED, expectedResponse);
    });

    test('rejects an non-moderator user', async () => {
      await knex('Users')
        .update({
          usergroup: 1,
        })
        .where('id', TEST_USER_ID);

      const data: UpdateRuleRequest = {
        description: 'New Description',
      };

      await api
        .put('/rules/1')
        .send(data)
        .set('Cookie', `knockoutJwt=${knockoutJwt}`)
        .expect(httpStatus.FORBIDDEN);

      await knex('Users')
        .update({
          usergroup: 5,
        })
        .where('id', TEST_USER_ID);
    });

    test('rejects a logged out user', async () => {
      const data: UpdateRuleRequest = {
        description: 'New Description',
      };

      await api.put('/rules/1').send(data).expect(httpStatus.UNAUTHORIZED);
    });
  });
});
