import httpStatus from 'http-status';
import jwt from 'jsonwebtoken';
import request from 'supertest';
import { APP_PORT, JWT_SECRET } from '../config/server';
import knex from '../src/services/knex';
import config from '../config/config';
import UserProfile from '../src/v2/schemas/users/userProfile';
import UpdateUserProfileRequest from '../src/v2/schemas/users/updateUserProfileRequest';

describe('/v2/users endpoint', () => {
  const { host } = config[process.env.NODE_ENV];
  const TEST_USER_ID = 990;

  const knockoutJwt = jwt.sign({ id: TEST_USER_ID }, JWT_SECRET, {
    algorithm: 'HS256',
    expiresIn: '2 weeks',
  });

  const api = request(`http://${host}:${APP_PORT}/v2/users`);

  beforeAll(async () => {
    await knex('Users').insert({
      id: TEST_USER_ID,
      username: 'Test User',
      avatar_url: 'none.webp',
      usergroup: 1,
      created_at: new Date(2020, 1, 1),
      updated_at: new Date(),
    });

    await knex('UserProfiles').insert({
      user_id: TEST_USER_ID,
      heading_text: 'stuff',
      personal_site: 'things',
    });
  });

  afterAll(async () => {
    await knex.raw('SET FOREIGN_KEY_CHECKS=0');
    await knex('Users').where('id', TEST_USER_ID).delete();
    await knex('UserProfiles').where('user_id', TEST_USER_ID).delete();
    await knex.raw('SET FOREIGN_KEY_CHECKS=1');
  });

  describe('GET /:id/profile', () => {
    test('retrieves a user profile', async () => {
      const expectedResponse: UserProfile = {
        id: TEST_USER_ID,
        bio: 'stuff',
        social: {
          website: 'things',
        },
        background: {
          url: null,
          type: null,
        },
      };

      await api.get(`/${TEST_USER_ID}/profile`).expect(httpStatus.OK, expectedResponse);
    });
  });

  describe('PUT /:id/profile', () => {
    afterAll(async () => {
      await knex('Users').insert({
        id: TEST_USER_ID,
        username: 'Test User',
        avatar_url: 'none.webp',
        usergroup: 1,
        created_at: new Date(2020, 1, 1),
        updated_at: new Date(),
      });

      await knex('UserProfiles')
        .update({
          heading_text: 'stuff',
          personal_site: 'things',
          twitter: null,
        })
        .where('user_id', TEST_USER_ID);
    });

    test('updates a user profile', async () => {
      const data: UpdateUserProfileRequest = {
        bio: 'many things',
        social: {
          website: 'https://knockout.chat/',
          twitter: 'stuff',
        },
      };

      await api
        .put(`/${TEST_USER_ID}/profile`)
        .send(data)
        .set('Cookie', `knockoutJwt=${knockoutJwt}`)
        .expect((response) => console.log(response))
        .expect(httpStatus.OK);

      const expectedResponse: UserProfile = {
        id: TEST_USER_ID,
        bio: 'many things',
        social: {
          website: 'https://knockout.chat/',
          twitter: 'stuff',
        },
        background: {
          url: null,
          type: null,
        },
      };

      await api.get(`/${TEST_USER_ID}/profile`).expect(httpStatus.OK, expectedResponse);
    });

    test('rejects a logged out user', async () => {
      const data: UpdateUserProfileRequest = {
        bio: 'many things',
        social: {
          website: 'https://knockout.chat/',
          twitter: 'stuff',
        },
      };

      await api.put(`/${TEST_USER_ID}/profile`).send(data).expect(httpStatus.UNAUTHORIZED);
    });

    test('rejects a user who is not the profile owner', async () => {
      const data: UpdateUserProfileRequest = {
        bio: 'many things',
        social: {
          website: 'https://knockout.chat/',
          twitter: 'stuff',
        },
      };

      await api
        .put(`/${TEST_USER_ID + 1}/profile`)
        .send(data)
        .set('Cookie', `knockoutJwt=${knockoutJwt}`)
        .expect(httpStatus.FORBIDDEN);
    });
  });
});
