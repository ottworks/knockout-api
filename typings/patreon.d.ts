
interface PatreonAccessToken { }
interface PatreonRefreshToken { }

type PatreonRequestSpec =
  | string
  | { url: string }
  ;

interface PatreonOAuthToken {
  access_token: PatreonAccessToken;
  token_type: 'Bearer';
  expires_in: number;
  refresh_token: PatreonRefreshToken;
  scope: string;
}

interface PatreonOAuthClient {
  getTokens: (code: string, redirectUri: string) => Promise<PatreonOAuthToken>;
  refreshToken: (token: PatreonRefreshToken) => Promise<PatreonOAuthToken>;
}

interface PatreonOAuthFactory {
  (clientId: string, clientSecret: string): PatreonOAuthClient;
}

interface PatreonAPIClient {
  (accessToken: PatreonAccessToken): (requestSpec: PatreonRequestSpec) => Promise<any>;
}

declare module 'patreon' {
  let oauth: PatreonOAuthFactory;
  let patreon: PatreonAPIClient;
  let jsonApiURL: unknown;
}
