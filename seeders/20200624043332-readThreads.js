'use strict';

import { date } from 'faker';

const READ_THREADS_PER_USER_MAX = 25;

const randomInt = (max) => {
  return Math.floor(Math.random() * (max - 1) + 1);
}

const randomReadThread = (userId, threadId) => {
  return {
    user_id: userId,
    thread_id: threadId,
    last_seen: date.past(1),
    created_at: date.past(2),
    updated_at: date.past(1),
  }
}

const randomReadThreads = (userIds, threadIds) => {
  const readThreads = userIds.map((userId) => {
    const threadCount = randomInt(READ_THREADS_PER_USER_MAX);

    let threadIdPool = threadIds.slice(0);
    return Array.from({ length: threadCount }).map(() => {
      const randomIdIndex = randomInt(threadIdPool.length);
      const threadId = threadIdPool[randomIdIndex];
      threadIdPool.splice(randomIdIndex, 1);
      return randomReadThread(userId, threadId);
    })
  })

  return [].concat(...readThreads);
}

export async function up(queryInterface, Sequelize) {
  const userIds = await queryInterface.sequelize.query(
    `SELECT id FROM Users`,
    { type: Sequelize.QueryTypes.SELECT }
  ).map((record) => record.id);

  const threadIds = await queryInterface.sequelize.query(
    `SELECT id FROM Threads`,
    { type: Sequelize.QueryTypes.SELECT }
  ).map((record) => record.id);

  return queryInterface.bulkInsert('ReadThreads', randomReadThreads(userIds, threadIds), {});
}

export function down(queryInterface, Sequelize) {
  return queryInterface.bulkDelete('ReadThreads', null, {});
}
