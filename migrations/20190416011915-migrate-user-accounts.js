"use strict";

module.exports = {
  /**
   * Migrate everyone to the new account table
   *
   * @param {import("sequelize").QueryInterface} queryInterface
   * @param {import("sequelize").SequelizeStatic} Sequelize
   */
  up: async (queryInterface, Sequelize) => {
    const users = await queryInterface.sequelize.query(
      `SELECT * FROM Users WHERE external_id IS NOT NULL`,
      { type: Sequelize.QueryTypes.SELECT }
    );
    for (const user of users) {
      // Check if user doesn't already have an external account
      const externalAccounts = await queryInterface.sequelize.query(
        `SELECT * FROM ExternalAccounts WHERE user_id = :userId AND provider = :provider`,
        { type: Sequelize.QueryTypes.SELECT, replacements: { userId: user.id, provider: user.external_type } }
      );
      if (externalAccounts.length === 0) {
        // Copy their account data to the new table
        console.log(`Found user to migrate: ${user.id}`);
        await queryInterface.sequelize.query(
          `INSERT INTO ExternalAccounts (provider, user_id, external_id) VALUES (:provider, :userId, :externalId)`,
          { type: Sequelize.QueryTypes.INSERT, replacements: { userId: user.id, provider: user.external_type, externalId: user.external_id } }
        )
      }
    }
  },
  down: (queryInterface, Sequelize) => {
    // Leave this blank
  }
};
