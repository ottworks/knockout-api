'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.removeIndex('Threads', 'threads_deleted_at');
    return queryInterface.addIndex('Threads', 
    {
      fields: [
        {
          attribute: 'deleted_at',
          order: 'DESC'
        },
        {
          attribute: 'updated_at',
          order: 'DESC'
        },
        {
          attribute: 'pinned',
          order: 'DESC'
        },
        {
          attribute: 'locked',
        },
      ]
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.addIndex('Threads', ['deleted_at']);
    return queryInterface.removeIndex(
      'Threads',
      'threads_deleted_at_updated_at_pinned_locked'
    );
  }
};
