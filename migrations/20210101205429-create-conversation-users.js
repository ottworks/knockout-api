'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('ConversationUsers', {
      conversation_id: {
        allowNull: false,
        type: Sequelize.BIGINT,
        references: {
          model: "Conversations",
          key: "id"
        }
      },
      user_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: "Users",
          key: "id"
        }
      }
    }).then(() => {
      return queryInterface.sequelize.query(
        'ALTER TABLE `ConversationUsers` ADD CONSTRAINT `conversationusers_pk` PRIMARY KEY (`conversation_id`, `user_id`)'
      )
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('ConversationUsers');
  }
};
