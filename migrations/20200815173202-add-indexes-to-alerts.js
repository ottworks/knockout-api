// @ts-check
'use strict';

// READ ME
// This creates indexes on Alerts, to try and reduce some of load.
// Please make sure that this migration is ran during a non-busy period
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addIndex('Alerts', ['thread_id'])
    await queryInterface.addIndex('Alerts', ['last_seen'])
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeIndex('Alerts', 'alerts_thread_id');
    await queryInterface.removeIndex('Alerts', 'alerts_last_seen');
  }
};
