'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('Conversations', 'latest_message_id', {
      allowNull: true,
      type: Sequelize.BIGINT,
      references: {
        model: 'Messages',
        key: 'id'
      }
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Conversations', 'latest_message_id');
  }
};

