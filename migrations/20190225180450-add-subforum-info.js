// @ts-check
'use strict';

/**
 * @param {import('sequelize').QueryInterface} queryInterface 
 * @param {string} subforumName 
 * @param {string} description 
 */
function setDescription(queryInterface, subforumName, description) {
  return queryInterface.sequelize.query(
    `update Subforums set description = :description where name = :subforumName`,
    { replacements: { subforumName, description } }
  );
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    // Add column
    await queryInterface.addColumn('Subforums', 'description', {
      type: Sequelize.STRING
    });
    // Fill column
    await Promise.all([
      setDescription(queryInterface, 'General Discussion', `Talk about life, the universe, and everything else`),
      setDescription(queryInterface, 'Game Generals', `Megathreads about a single game`),
      setDescription(queryInterface, 'Gaming', `Everything else about gaming, including news and discussions`),
      setDescription(queryInterface, 'Videos', `In from the start and not going anywhere`),
      setDescription(queryInterface, 'Politics', `Yell at each other about Trump and AOC`),
      setDescription(queryInterface, 'News', `The latest news, selected by our finest shitposters`),
      setDescription(queryInterface, 'Film, Television and Music', `This month's boring superhero movie, Friends and anime`),
      setDescription(queryInterface, 'Hardware & Software', `Software you're making, hardware you're using`),
      setDescription(queryInterface, 'Meta', `Tell us how great we are`)
    ]);
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Subforums', 'description');
  }
};
