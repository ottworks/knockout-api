'use strict';

const DEFAULT_RULES = [
  {
    title: 'Use English',
    description:
      'Certain threads may be multilingual when directed, however, English is the most used language on the forums.',
  },
  {
    title: 'Read Before You Post',
    description:
      'Please read what the thread is about and what users have said before making your post. Sometimes you may end up repeating what someone else has said, and that’s okay. But, it is not okay to ignore the OP.',
  },
  {
    title: 'Thread Necromancy',
    description:
      'Don’t bump threads unless you’re adding content or requesting status updates on projects. Going “any news?” is not okay.',
  },
  {
    title: 'Warez',
    description:
      'Don’t link. You can discuss piracy but don’t brag about pirating X thing. Posting Abandonware is okay if you legally cannot purchase said content. Check My Abandonware before posting.',
  },
  {
    title: 'Don’t Flame',
    description: 'Refrain from calling someone an idiot or insulting them directly (no matter how benign the insult). Even if they’re being an idiot. Be tactful and respectful when talking to other users, even if you disagree with them.'
  },
  {
    title: 'Don’t Have A NSFW Avatar',
    description:
      'Don’t use avatars that are or contain the following: Porn, suggestive, cropped, blurred. Basically think of how a “middle aged Christian soccer mom boss” would think if they saw it. If you skirt the borderlines you will be contacted by a moderator to find a compromise. Moderators also reserve the right to remove your avatar and background. We don’t care what it is as long as it doesn’t break the above.',
  },
  {
    title: 'Don’t Troll Or Uphold Bad Faith Arguments',
    description:
      'Don’t be vague, piss around, take no care in posting, post in bad faith to irritate your fellow users. We won’t give out examples. But we will catch you if you break this.',
  },
  {
    title: 'Report Rule Breakers',
    description:
      'Report posts that break any of the rules. Don’t report people because you disagree with them.',
  },
  {
    title: 'Bigotry Is Not Welcomed Here. Period',
    description: 'No racism. No sexism. No phobia.',
  },
  {
    title: 'Follow Thread OP Rules',
    description: 'Some threads have their own specific set of rules in the OP. Please do read them.',
  },
  {
    title: 'Sobriety',
    description:
      'You won’t get banned if you are under the influence. But you shouldn’t let it disrupt the flow of threads and posts.',
  },
  {
    title: "Don't Do Callouts",
    description: 'Don’t do this. It’s obnoxious and doesn’t contribute in anyway.',
  },
  {
    title: 'Don’t Be A Dick',
    description: 'This doesn’t need explaining.',
  },
  {
    title: "Don't Even Think About Raiding",
    description:
      'Don’t even think about doing this or encouraging this. Raiding other places/discord servers/other forums to cause a mess is going to get you banned there and here. It’s not tolerated. Period.',
  },
  {
    title: "Don't Advocate Violence",
    description:
      'Do not advocate for violence or harm towards anyone. There is zero tolerance for that behavior. If you’re angry at someone, you can express it in other ways. Levelheadedness is expected and required of every user.',
  },
  {
    title: 'NSFW Content',
    description:
      'All NSFW content must be limited to threads tagged NSFW. Do not post NSFW content outside properly tagged threads. If you’re creating a thread that does/might contain NSFW content, tag it so or you will be awarded a severe ban.',
  },
];

const NEWS_RULES = [
  {
    title: 'Keep It Fresh',
    description: 'Sources should be less than 2 weeks old.',
  },
  {
    title: 'Don’t Editoralise',
    description:
      'You should also not change the title too much to a point where it doesn’t fit the original title (this prevents click-baity headlines but throws responsibility on the OP to get it right, use your judgement).',
  },
  {
    title: 'Report',
    description:
      'Report posts that break any of the rules. Don’t report people because you disagree with them.',
  },
  {
    title: 'Keep A Cool Head',
    description:
      'If things get heated take a step back.(I don’t expect this rule to be used often or at all but expect us mods to roll into a thread and tell people too chill)',
  },
  {
    title: 'Be Funny When It’s Appropriate',
    description:
      'You can crack epic zingers and jokes but in national tragedies and high profile stories we expect you to keep them to yourselves. (aka don’t make jokes about dead people)',
  },
  {
    title: 'Don’t Post Manifestos Or Hate Rhetoric',
    description: 'We don’t want to harbor or be a source.',
  },
  {
    title: 'BREAKING',
    description: 'Don’t put breaking in your titles. Us moderators will make that call.',
  },
  {
    title: 'Updating',
    description:
      'If you are the op. It will be useful if you kept your OP full of updates linking to posts during high profile, breaking headlines.',
  },
  {
    title: 'Opinions',
    description: 'Opinion pieces are not news.',
  },
  {
    title: 'Satire',
    description: 'Satire pieces are not okay. Don’t use tabloids either.',
  },
  {
    title: 'Paywalls',
    description:
      'Don’t post paywall articles. If you must, you have to present an alternative source as well your paywall.',
  },
];

const POLITICS_RULES = [
  {
    title: "Keep It Classy And Informative. Don't Try To Be Funny.",
    description:
      'Maintain a high quality posting standard. Don’t shitpost. If you crack a joke you run the risk of being banned(in other words, don’t piss off the entire thread with your sick epic one liner).',
  },
  {
    title: 'Neutrality',
    description:
      'When creating a thread. Use the most neutral and factual source. We aren’t going to make a list or use a premade one. We expect you to use your judgement here. It is safer to follow up with an additional source as your backup if your original is bogus.',
  },
  {
    title: 'Honesty',
    description:
      'Don’t be wishy washy, vague or up hold bad faith. You will piss off everyone and you will be banned.',
  },
  {
    title: 'Image Macros',
    description: 'One per page per user. There are no exceptions.',
  },
  {
    title: 'Opinions',
    description:
      'You can make opinion piece threads but you must post a relevant source that the opinion article is talking about AND you’re allowed broad/general opinions. You won’t be banned unless you intentionally break this.',
  },
];

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const siteRules = DEFAULT_RULES.map((rule, cardinality) => {
      return {
        rulable_type: null,
        rulable_id: null,
        category: 'Site Wide Rules',
        title: rule.title,
        description: rule.description,
        cardinality
      }
    });

    const newsRules = await queryInterface.sequelize.query(
      `SELECT id FROM Subforums WHERE name = :name LIMIT 1`,
      { type: Sequelize.QueryTypes.SELECT, replacements: { name: 'News' }}
    )
      .then(record => NEWS_RULES.map((rule, cardinality) => {
        return {
          rulable_type: 'Subforum',
          rulable_id: record[0].id,
          category: 'General Rules',
          title: rule.title,
          description: rule.description,
          cardinality
        }
      }));

    const politicsRules = await queryInterface.sequelize.query(
        `SELECT id FROM Subforums WHERE name = :name LIMIT 1`,
        { type: Sequelize.QueryTypes.SELECT, replacements: { name: 'Politics' }}
      )
      .then(record => POLITICS_RULES.map((rule, cardinality) => {
        return {
          rulable_type: 'Subforum',
          rulable_id: record[0].id,
          category: 'General Rules',
          title: rule.title,
          description: rule.description,
          cardinality
        }
      }));

    const allRules = siteRules.concat(newsRules, politicsRules);

    return queryInterface.bulkInsert('Rules', allRules);
  },

  down: (queryInterface, Sequelize) => {
    // noop here, we're deleting the Rules table in the previous migration anyway
  }
};
