import AbstractRetriever from './abstractRetriever';
import knex from '../services/knex';
import Message from './message';
import { getFormattedObjects as getUserObjects } from './user';

export default class Conversation extends AbstractRetriever {
  protected cacheLifetime = 86400;

  private cachePrefix: string = 'conversation';

  public static RETRIEVE_SHALLOW = 1;

  private static async getConversations(ids: Array<number>) {
    const results = await knex
      .from('Conversations as c')
      .select(
        'c.id as conversationId',
        'c.created_at as conversationCreatedAt',
        'c.updated_at as conversationUpdatedAt'
      )
      .whereIn('c.id', ids);

    return results.reduce((list, conversation) => {
      list[conversation.conversationId] = conversation;
      return list;
    }, {});
  }

  private static format(data): Object {
    return {
      id: data.conversationId,
      messages: [],
      users: data.users,
      createdAt: data.conversationCreatedAt,
      updatedAt: data.conversationUpdatedAt,
    };
  }

  private allMessagesQuery = () =>
    knex
      .from('Messages')
      .select('id', 'conversation_id as conversationId')
      .whereIn('conversation_id', this.ids)
      .orderBy('created_at', 'desc');

  private latestMessagesQuery = () =>
    knex
      .from('Conversations as c')
      .select('c.latest_message_id as id', 'c.id as conversationId')
      .whereIn('c.id', this.ids)
      .orderBy('updated_at', 'desc');

  private static async getUsers(ids) {
    const conversationUsers = await knex('ConversationUsers')
      .select('user_id', 'conversation_id')
      .whereIn('conversation_id', ids);

    const users = await getUserObjects(conversationUsers.map((item) => item.user_id));
    const userMap = users.reduce((output, user) => {
      output[user.id] = user;
      return output;
    }, {});

    const convoToUsers = conversationUsers.reduce((output, convoUser) => {
      if (typeof output[convoUser.conversation_id] === 'undefined')
        output[convoUser.conversation_id] = [];
      output[convoUser.conversation_id].push(userMap[convoUser.user_id]);
      return output;
    }, {});
    return convoToUsers;
  }

  private async getMessages() {
    let conversationMessages;
    if (this.hasFlag(Conversation.RETRIEVE_SHALLOW)) {
      conversationMessages = await this.latestMessagesQuery();
    } else {
      conversationMessages = await this.allMessagesQuery();
    }

    const messageIds = conversationMessages.map((message) => message.id);
    const messageRetriever = new Message(messageIds, []);
    const messages = await messageRetriever.get();
    const messageIndex = messages.reduce((list, message) => {
      list[message.id] = message;
      return list;
    }, {});
    return conversationMessages.reduce((list, message) => {
      if (typeof list[message.conversationId] === 'undefined') list[message.conversationId] = [];
      list[message.conversationId].push(messageIndex[message.id]);
      return list;
    }, {});
  }

  async get() {
    // grab canonical data
    const cachedConversations = await this.cacheGet(this.cachePrefix, this.ids);
    const uncachedIds = this.filterNullIndices(cachedConversations);
    const uncachedConversations = await Conversation.getConversations(uncachedIds);
    const users = await Conversation.getUsers(uncachedIds);
    uncachedIds.forEach((id) => {
      uncachedConversations[id].users = users[id] || [];
    });

    const conversations = this.ids.map((id, index) => {
      if (cachedConversations[index] !== null) return cachedConversations[index];
      return Conversation.format(uncachedConversations[id]);
    });

    // write formatted data back to the cache
    if (uncachedIds.length > 0) {
      conversations.map(async (conversation) => {
        if (conversation) {
          await this.cacheSet(this.cachePrefix, conversation.id, conversation);
        }
      });
    }

    // grab data from related caches
    const messages = await this.getMessages();

    // merge related data in
    return conversations.reduce((result, conversation) => {
      if (conversation) {
        conversation.messages = messages[conversation.id] || [];
        result.push(conversation);
      }
      return result;
    }, []);
  }

  async invalidate() {
    this.ids.map(async (id) => {
      await this.cacheDrop(this.cachePrefix, id);
    });
  }
}

export const invalidateObjects = async (ids: Array<number>) => {
  const conversationRetriever = new Conversation(ids, []);
  await conversationRetriever.invalidate();
};

export const invalidateObject = async (id: number) => {
  const conversationRetriever = new Conversation([id], []);
  await conversationRetriever.invalidate();
};

export const getFormattedObjects = async (ids: Array<number>, flags: Array<number> = []) => {
  const conversationRetriever = new Conversation(ids, flags);
  return conversationRetriever.get();
};

export const getFormattedObject = async (id: number, flags: Array<number> = []) => {
  const conversationRetriever = new Conversation([id], flags);
  const threads = await conversationRetriever.get();
  return threads[0];
};
