/* eslint-disable import/no-cycle */
import AbstractRetriever from './abstractRetriever';
import User from './user';
import Post from './post';
import Thread from './thread';
import knex from '../services/knex';

export default class Ban extends AbstractRetriever {
  private cachePrefix: string = 'ban';

  public static INCLUDE_POST = 1;

  public static INCLUDE_THREAD = 2;

  private static async getBans(ids: Array<number>) {
    const results = await knex
      .from('Bans as b')
      .select(
        'b.id as banId',
        'b.user_id as banUserId',
        'b.expires_at as banExpiresAt',
        'b.created_at as banCreatedAt',
        'b.updated_at as banUpdatedAt',
        'b.ban_reason as banReason',
        'b.banned_by as banBannedBy',
        'b.post_id as banPostId',
        knex.raw('(select p.thread_id from Posts as p where p.id = b.post_id) as banThreadId')
      )
      .whereIn('b.id', ids);

    return results.reduce((list, ban) => {
      list[ban.banId] = ban;
      return list;
    }, {});
  }

  private static async getUsers(bans: Array<any>) {
    const userIds = bans
      .reduce((list, ban) => {
        list.push(ban.bannedBy);
        list.push(ban.user);
        return list;
      }, [])
      .filter((elem, index, self) => index === self.indexOf(elem));

    const userRetriever = new User(userIds, []);
    const rawUsers = await userRetriever.get();
    return rawUsers.reduce((list, user) => {
      list[user.id] = user;
      return list;
    }, {});
  }

  private async getPosts(bans: Array<any>) {
    if (!this.hasFlag(Ban.INCLUDE_POST)) return [];

    const postIds = bans
      .map((ban) => ban.post)
      .filter((elem, index, self) => index === self.indexOf(elem));

    const postRetriever = new Post(postIds, [Post.RETRIEVE_SHALLOW]);
    const rawPosts = await postRetriever.get();
    return rawPosts.reduce((list, post) => {
      list[post.id] = post;
      return list;
    }, {});
  }

  private async getThreads(bans: Array<any>) {
    if (!this.hasFlag(Ban.INCLUDE_THREAD)) return [];

    const threadIds = bans
      .map((ban) => ban.thread)
      .filter((elem, index, self) => index === self.indexOf(elem));

    const threadRetriever = new Thread(threadIds, [Thread.RETRIEVE_SHALLOW]);
    const rawThreads = await threadRetriever.get();
    return rawThreads.reduce((list, thread) => {
      list[thread.id] = thread;
      return list;
    }, {});
  }

  private static format(data): Object {
    return {
      id: data.banId,
      banReason: data.banReason,
      expiresAt: data.banExpiresAt,
      createdAt: data.banCreatedAt,
      updatedAt: data.banUpdatedAt,
      post: data.banPostId,
      user: data.banUserId,
      bannedBy: data.banBannedBy,
      thread: data.banThreadId,
    };
  }

  async get() {
    // grab canonical data
    const cachedBans = await this.cacheGet(this.cachePrefix, this.ids);
    const uncachedIds = this.filterNullIndices(cachedBans);
    const uncachedBans = await Ban.getBans(uncachedIds);
    const bans = this.ids.map((id, index) => {
      if (cachedBans[index] !== null) return cachedBans[index];
      return Ban.format(uncachedBans[id]);
    });

    // write formatted data back to the cache
    if (uncachedIds.length > 0) {
      bans.map(async (ban) => {
        await this.cacheSet(this.cachePrefix, ban.id, ban);
      });
    }

    // grab data from related caches
    const users = await Ban.getUsers(bans);
    const posts = await this.getPosts(bans);
    const threads = await this.getThreads(bans);

    // merge related data in
    return bans.map((ban) => {
      ban.user = users[ban.user] || ban.user;
      ban.bannedBy = users[ban.bannedBy] || ban.bannedBy;
      ban.post = posts[ban.post] || ban.post;
      ban.thread = threads[ban.thread] || ban.thread;
      return ban;
    });
  }

  async invalidate() {
    this.ids.map(async (id) => {
      await this.cacheDrop(this.cachePrefix, id);
    });
  }
}

export const invalidateObjects = async (ids: Array<number>) => {
  const banRetriever = new Ban(ids, []);
  await banRetriever.invalidate();
};

export const invalidateObject = async (id: number) => {
  const banRetriever = new Ban([id], []);
  await banRetriever.invalidate();
};

export const getFormattedObjects = async (ids: Array<number>) => {
  const banRetriever = new Ban(ids, []);
  return banRetriever.get();
};

export const getFormattedObject = async (id: number) => {
  const banRetriever = new Ban([id], []);
  const bans = await banRetriever.get();
  return bans[0];
};
