import SteamAPI from 'steamapi';
import AbstractRetriever from './abstractRetriever';
import knex from '../services/knex';
import UserProfile from '../v2/schemas/users/userProfile';
import { STEAM_API_KEY } from '../../config/server';

export default class Profile extends AbstractRetriever {
  protected cacheLifetime = 86400;

  private cachePrefix: string = 'profile';

  private static async getProfiles(ids: Array<number>) {
    const results = await knex
      .from('UserProfiles as p')
      .select(
        'p.user_id as userId',
        'p.heading_text as headingText',
        'p.personal_site as personalSite',
        'p.background_url as backgroundUrl',
        'p.background_type as backgroundType',
        'p.steam as steam',
        'p.discord as discord',
        'p.github as github',
        'p.youtube as youtube',
        'p.twitter as twitter',
        'p.twitch as twitch',
        'p.gitlab as gitlab',
        'p.tumblr as tumblr',
        'p.created_at as createdAt',
        'p.updated_at as updatedAt'
      )
      .whereIn('p.user_id', ids);

    return results.reduce((list, profile) => {
      list[profile.userId] = profile;
      return list;
    }, {});
  }

  private static async getSteamNickname(url: string) {
    try {
      const steam = new SteamAPI(STEAM_API_KEY);
      const id = await steam.resolve(url);
      const summary = await steam.getUserSummary(id);
      return summary.nickname;
    } catch (error) {
      console.error(error);
      return undefined;
    }
  }

  private static getDefined(property) {
    if (property?.length > 0) {
      return property;
    }
    return undefined;
  }

  private static async format(data): Promise<UserProfile> {
    return {
      id: data.userId,
      bio: data.headingText,
      social: {
        website: this.getDefined(data.personalSite),
        steam: this.getDefined(data.steam)
          ? {
              name: await Profile.getSteamNickname(data.steam),
              url: data.steam,
            }
          : undefined,
        discord: this.getDefined(data.discord),
        github: this.getDefined(data.github),
        youtube: this.getDefined(data.youtube),
        twitter: this.getDefined(data.twitter),
        twitch: this.getDefined(data.twitch),
        gitlab: this.getDefined(data.gitlab),
        tumblr: this.getDefined(data.tumblr),
      },
      background: {
        url: data.backgroundUrl,
        type: data.backgroundType,
      },
    };
  }

  async get(): Promise<UserProfile[]> {
    // grab canonical data
    const cachedProfiles = await this.cacheGet(this.cachePrefix, this.ids);
    const uncachedIds = this.filterNullIndices(cachedProfiles);
    const uncachedProfiles = await Profile.getProfiles(uncachedIds);
    const profiles = this.ids.map((id, index) => {
      if (cachedProfiles[index] !== null) return cachedProfiles[index];
      if (typeof uncachedProfiles[id] !== 'undefined') return Profile.format(uncachedProfiles[id]);
      return {};
    });

    // write formatted data back to the cache
    if (uncachedIds.length > 0) {
      profiles.map(async (profile) => {
        await this.cacheSet(this.cachePrefix, profile.id, profile);
      });
    }

    return profiles;
  }

  async invalidate() {
    this.ids.map(async (id) => {
      await this.cacheDrop(this.cachePrefix, id);
    });
  }
}

// legacy signitures, remove at soonest convenience
export const invalidateObjects = async (ids: Array<number>) => {
  const profileRetriever = new Profile(ids, []);
  await profileRetriever.invalidate();
};

export const invalidateObject = async (id: number) => {
  const profileRetriever = new Profile([id], []);
  await profileRetriever.invalidate();
};

export const getFormattedObjects = async (ids: Array<number>) => {
  const profileRetriever = new Profile(ids, []);
  return profileRetriever.get();
};

export const getFormattedObject = async (id: number) => {
  const profileRetriever = new Profile([id], []);
  const profiles = await profileRetriever.get();
  return profiles[0];
};
