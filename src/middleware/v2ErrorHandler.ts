/* eslint-disable class-methods-use-this */
import { NextFunction } from 'express';
import httpStatus from 'http-status';
import {
  BadRequestError,
  ExpressErrorMiddlewareInterface,
  HttpError,
  Middleware,
} from 'routing-controllers';
import errorHandler from '../services/errorHandler';

@Middleware({ type: 'after' })
export default class ErrorHandler implements ExpressErrorMiddlewareInterface {
  error(error: any, request: any, response: any, next: NextFunction): void {
    if (error instanceof BadRequestError || error instanceof HttpError) {
      response.status(error.httpCode);
      response.json({
        name: error.name,
        stack: error.stack,
        errors: ({ ...error } as any).errors,
      });
      return next();
    }
    return errorHandler.respondWithError(httpStatus.INTERNAL_SERVER_ERROR, error, response);
  }
}
