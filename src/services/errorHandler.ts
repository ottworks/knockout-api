import { Request, Response, NextFunction, RequestHandler } from 'express';
import ResponseStrategy from '../helpers/responseStrategy';

/*
  Catch Errors Handler

  With async/await, you need some way to catch errors
  Instead of using try{} catch(e) {} in each controller, we wrap the function in
  catchErrors(), catch and errors they throw, and pass it along to our express middleware with next()
*/
const catchErrors = (fn: RequestHandler) =>
  function (req: Request, res: Response, next: NextFunction) {
    return fn(req, res, next).catch(next);
  };

// Always remember to exec this after all routes on the app
const catchHandler = (app) => {
  app.use((_req, res) => {
    ResponseStrategy.send(res, null, null);
  });

  app.use((err, _req, res) => {
    ResponseStrategy.send(res, err, null);
  });
};

const respondWithError = (
  statusCode: number,
  exception: Error,
  res: Response,
  message?: string
) => {
  res.status(statusCode);
  res.json({
    message,
    error: exception.message,
    stackTrace: exception.stack,
  });
};

export default {
  catchErrors,
  catchHandler,
  respondWithError,
};
