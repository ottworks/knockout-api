import config from "../../config/config";
import knex from "knex";

const { host, port, username, password, database } = config[process.env.NODE_ENV];

const knexClient = knex({
  client: "mysql",
  connection: {
    host,
    port,
    user: username,
    password,
    database,
    charset: "utf8mb4"
  }
});

// New export
export default knexClient;

// Backwards-compatible export
module.exports.knex = knexClient;
