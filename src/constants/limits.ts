export const POST_CHARACTER_LIMIT: number = 100000;
export const IMAGE_MAX_FILESIZE_BYTES: number = 5000000;
export const MESSAGE_CHARACTER_LIMIT: number = 100000;
export const THREAD_POST_LIMIT: number = 1000;
export const NEWLINE_MULTIPLIER: number = 65;
