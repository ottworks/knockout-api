"use strict";

module.exports = (sequelize, DataTypes) => {
  const IpAddress = sequelize.define(
    "IpAddress",
    {
      ip_address: DataTypes.STRING,
      user_id: DataTypes.INTEGER,
      post_id: DataTypes.INTEGER.UNSIGNED,
      created_at: {
        type: DataTypes.DATE, field: 'created_at'
      },
    }, 
    {
      timestamps: false
    }
  );
  IpAddress.associate = (models) => {
    IpAddress.User = IpAddress.belongsTo(
      models.User, 
      { as: 'user', foreignKey: 'user_id' }
    );
    IpAddress.Post = IpAddress.belongsTo(
      models.Post,
      { as: 'post', foreignKey: 'post_id' }
    );
  }
  IpAddress.initScopes = () => {};
  return IpAddress;
};
