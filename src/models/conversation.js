'use strict';

module.exports = (sequelize, DataTypes) => {
  const Conversation = sequelize.define('Conversation', {
    latest_message_id: {
      type: DataTypes.BIGINT,
      references: {
        model: "Messages",
        key: "id"
      }
    },
    created_at: {
      type: DataTypes.DATE, field: 'created_at'
    },
    updated_at: {
      type: DataTypes.DATE, field: 'created_at'
    }
  }, {
    timestamps: true
  });
  Conversation.associate = () => {};
  Conversation.initScopes = () => {};
  return Conversation;
};