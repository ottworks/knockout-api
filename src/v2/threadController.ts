/* eslint-disable class-methods-use-this */
import { Request, Response } from 'express';
import httpStatus from 'http-status';
import {
  Get,
  JsonController,
  UseBefore,
  Req,
  Res,
  Post,
  Body,
  Put,
  Param,
  HttpCode,
} from 'routing-controllers';
import { OpenAPI, ResponseSchema } from 'routing-controllers-openapi';
import { threadController } from '../controllers';
import { authentication } from '../middleware/auth';
import { rateLimiterUserMiddleware } from '../middleware/rateLimit';
import OpenAPIParam from './schemas/openAPIParam';
import CreateThreadRequest from './schemas/threads/createThreadRequest';
import NewThread from './schemas/threads/newThread';
import Thread from './schemas/threads/thread';
import ThreadWithLastPost from './schemas/threads/threadWithLastPost';
import ThreadWithPosts from './schemas/threads/threadWithPosts';
import ThreadWithRecentPosts from './schemas/threads/threadWithRecentPosts';
import UpdateThreadRequest from './schemas/threads/updateThreadRequest';
import UpdateThreadTagsRequest from './schemas/threads/updateThreadTagsRequest';

@OpenAPI({ tags: ['Threads'] })
@JsonController('/threads')
export default class ThreadController {
  @Get('/latest')
  @ResponseSchema(ThreadWithLastPost, { isArray: true })
  async getLatestThreads() {
    return threadController.latest();
  }

  @Get('/popular')
  @ResponseSchema(ThreadWithRecentPosts, { isArray: true })
  async getPopularThreads() {
    return threadController.popular();
  }

  @Get('/:id/:page?')
  @UseBefore(authentication.optional)
  @OpenAPIParam('id', { description: 'The id of the thread.' })
  @OpenAPIParam('page', { description: 'The page of the thread.' })
  @ResponseSchema(ThreadWithPosts)
  getThreadPosts(@Req() request: Request) {
    return threadController.getPostsAndCount(request);
  }

  @Post('/')
  @UseBefore(...[authentication.required, rateLimiterUserMiddleware])
  @OpenAPI({ summary: 'Create a thread' })
  @HttpCode(httpStatus.CREATED)
  @ResponseSchema(NewThread)
  async createThread(
    @Req() request: Request,
    @Res() response: Response,
    @Body() body: CreateThreadRequest
  ) {
    /* eslint-disable @typescript-eslint/naming-convention */
    const { icon_id, user_id, subforum_id, background_url, background_type, ...result } =
      await threadController.store(request, response, body);
    /* eslint-enable @typescript-eslint/naming-convention */

    return {
      ...result,
      iconId: icon_id,
      userId: user_id,
      subforumId: subforum_id,
      backgroundUrl: background_url,
      backgroundType: background_type,
    };
  }

  @Put('/:id')
  @UseBefore(authentication.required)
  @OpenAPI({ summary: 'Update a thread' })
  @OpenAPIParam('id', { description: 'The id of the thread.' })
  @ResponseSchema(Thread)
  updateThread(
    @Param('id') id: number,
    @Req() request: Request,
    @Body() body: UpdateThreadRequest
  ) {
    return threadController.update(id, body, request.user);
  }

  @Put('/:id/tags')
  @UseBefore(authentication.required)
  @OpenAPIParam('id', { description: 'The id of the thread.' })
  async updateThreadTags(
    @Param('id') id: number,
    @Req() request: Request,
    @Body() body: UpdateThreadTagsRequest
  ) {
    await threadController.updateTags(id, body, request.user);
    return { message: 'Tags updated.' };
  }
}
