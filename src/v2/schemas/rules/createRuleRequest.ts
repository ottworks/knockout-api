import { IsIn, IsInt, IsOptional, IsPositive, IsString, Length } from 'class-validator';
import { JSONSchema } from 'class-validator-jsonschema';

export default class CreateRuleRequest {
  @IsOptional()
  @IsIn(['Subforum', 'Thread'])
  @IsString()
  @JSONSchema({ description: 'The resource name this rule applies to.' })
  rulable_type?: 'Subforum' | 'Thread';

  @IsOptional()
  @IsPositive()
  @IsInt()
  @JSONSchema({ description: 'The ID of the resource this rule applies to.' })
  rulable_id?: number;

  @Length(3, 140)
  @JSONSchema({ description: 'The category of the rule.' })
  category: string;

  @Length(3, 140)
  @JSONSchema({ description: 'The title of the rule.' })
  title: string;

  @IsPositive()
  @IsInt()
  @JSONSchema({ description: 'The cardinality of the rule relative to other rules.' })
  cardinality: number;

  @Length(10, 1000)
  @JSONSchema({ description: 'The description of the rule.' })
  description: string;
}
