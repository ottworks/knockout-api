import { Type } from 'class-transformer';
import { MaxLength, ValidateNested } from 'class-validator';
import UserProfileSocial from './userProfileSocial';

export default class UpdateUserProfileRequest {
  @MaxLength(160)
  bio: string;

  @ValidateNested()
  @Type(() => UserProfileSocial)
  social: UserProfileSocial;
}
