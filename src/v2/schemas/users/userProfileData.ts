import { ValidateNested } from 'class-validator';
import UserProfileBackground from './userProfileBackground';
import UpdateUserProfileRequest from './updateUserProfileRequest';

export default class UserProfileData extends UpdateUserProfileRequest {
  @ValidateNested()
  background: UserProfileBackground;
}
