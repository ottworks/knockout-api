import { IsInt } from 'class-validator';
import UserProfileData from './userProfileData';

export default class UserProfile extends UserProfileData {
  @IsInt()
  id: number;
}
