import { IsBase64, IsIn, IsOptional } from 'class-validator';

export default class UserProfileBackgroundRequest {
  @IsOptional()
  @IsBase64()
  image?: string;

  @IsOptional()
  @IsIn(['cover', 'tiled'])
  type?: 'cover' | 'tiled';
}
