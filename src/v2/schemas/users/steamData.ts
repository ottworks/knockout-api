import { IsOptional, IsString, IsUrl, ValidateIf } from 'class-validator';

export default class SteamData {
  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @ValidateIf((e) => e.url !== '')
  @IsUrl({ require_protocol: true, host_whitelist: ['steamcommunity.com'] })
  url: string;
}
