import { IsInt } from 'class-validator';

export default class Viewers {
  @IsInt()
  memberCount: number;

  @IsInt()
  guestCount: number;
}
