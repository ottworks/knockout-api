/* eslint-disable class-methods-use-this */
import {
  Get,
  JsonController,
  UseBefore,
  Param,
  Put,
  Body,
  UploadedFile,
  Req,
  ForbiddenError,
} from 'routing-controllers';
import { OpenAPI, ResponseSchema } from 'routing-controllers-openapi';
import multer from 'multer';
import { Request } from 'express';
import { authentication } from '../middleware/auth';
import OpenAPIParam from './schemas/openAPIParam';
import { userProfileController } from '../controllers';
import UserProfile from './schemas/users/userProfile';
import UpdateUserProfileRequest from './schemas/users/updateUserProfileRequest';
import UserProfileBackgroundRequest from './schemas/users/userProfileBackgroundRequest';

@OpenAPI({ tags: ['Users'] })
@JsonController('/users')
export default class UserController {
  @Get('/:id/profile')
  @UseBefore(authentication.optional)
  @OpenAPI({ summary: 'Get a user profile' })
  @OpenAPIParam('id', { description: 'The id of the user.' })
  @ResponseSchema(UserProfile)
  getUserProfile(@Param('id') id: number): Promise<UserProfile> {
    return userProfileController.get(id);
  }

  @Put('/:id/profile')
  @UseBefore(authentication.required)
  @OpenAPI({ summary: 'Update a user profile' })
  @OpenAPIParam('id', { description: 'The id of the user.' })
  updateUserProfile(
    @Param('id') id: number,
    @Req() request: Request,
    @Body() body: UpdateUserProfileRequest
  ) {
    if (id !== request.user.id) throw new ForbiddenError();
    return userProfileController.update(id, body);
  }

  @Put('/:id/profile/background')
  @UseBefore(authentication.required)
  @OpenAPI({
    summary: "Update a user profile's background",
    requestBody: {
      content: {
        'multipart/form-data': {
          schema: { $ref: '#/components/schemas/UserProfileBackgroundRequest' },
        },
      },
    },
  })
  @OpenAPIParam('id', { description: 'The id of the user.' })
  updateUserProfileBackground(
    @Param('id') id: number,
    @Req() request: Request,
    @UploadedFile('image', { options: { storage: multer.memoryStorage() } }) file: any,
    @Body() body: UserProfileBackgroundRequest
  ) {
    if (id !== request.user.id) throw new ForbiddenError();

    return userProfileController.updateBackground(file, request.user, body.type);
  }
}
