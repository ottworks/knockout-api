import { Request, Response } from 'express';
import ResponseStrategy from '../helpers/responseStrategy';
import knex from '../services/knex';
import redis from '../services/redisClient';

// eslint-disable-next-line import/prefer-default-export
export const index = async (req: Request, res: Response) => {
  const cacheKey = 'stats';
  let stats: any = await redis.getAsync(cacheKey);
  if (typeof stats === 'string') {
    stats = JSON.parse(stats);
  } else {
    // the queries below are only estimates since we use a faster, less accurate way of getting
    // the counts of each table
    const query = await knex
      .select(
        knex.raw(
          '(SELECT SUM(TABLE_ROWS) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = "Users") as userCount'
        ),
        knex.raw(
          '(SELECT SUM(TABLE_ROWS) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = "Posts") as postCount'
        ),
        knex.raw(
          '(SELECT SUM(TABLE_ROWS) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = "Threads") as threadCount'
        ),
        knex.raw(
          '(SELECT SUM(TABLE_ROWS) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = "Ratings") as ratingsCount'
        )
      )
      .first();

    const { userCount, postCount, threadCount, ratingsCount } = query;

    stats = {
      userCount,
      postCount,
      threadCount,
      ratingsCount,
    };

    redis.setex(cacheKey, 3600, JSON.stringify(stats));
  }

  return ResponseStrategy.send(res, stats);
};
