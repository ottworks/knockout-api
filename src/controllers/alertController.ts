import { Request, Response } from 'express';
import httpStatus from 'http-status';
import onDuplicateUpdate from '../helpers/onDuplicateUpdate';
import knex from '../services/knex';
import { alertsPerPage } from '../constants/pagination';
import { MODERATOR_GROUPS } from '../constants/userGroups';
import Alert, { getFormattedObjects } from '../retriever/alert';
import errorHandler from '../services/errorHandler';

export const store = async (req: Request, res: Response) => {
  try {
    if (!req.isLoggedIn) {
      throw new Error('Bad user login on alertController.store');
    }

    onDuplicateUpdate(knex, 'Alerts', {
      user_id: req.user.id,
      thread_id: req.body.threadId,
      last_seen: req.body.lastSeen ? new Date(req.body.lastSeen) : new Date(),
      last_post_number: req.body.lastPostNumber,
    });

    res.status(httpStatus.CREATED);
    res.json({ message: `Created an alert for thread #${req.body.threadId}` });
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const destroy = async (req: Request, res: Response) => {
  try {
    if (!req.isLoggedIn) {
      throw new Error('Bad user login on alertController.destroy');
    }

    await knex('Alerts').where({ user_id: req.user.id, thread_id: req.body.threadId }).del();

    res.status(httpStatus.OK);
    res.json({ message: `Deleted alert for thread #${req.body.threadId}` });
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const getAlertsQuery = async (
  userId: number,
  userUsergroup: number,
  hideNsfw: boolean = false
) => {
  const userIsMod = MODERATOR_GROUPS.includes(userUsergroup);

  const query = await knex('Alerts as al')
    .select('al.thread_id as thread_id')
    .where('al.user_id', [userId]);

  const tags = [];
  if (userIsMod) {
    tags.push(Alert.HIDE_DELETED_THREADS);
  }

  if (hideNsfw) {
    tags.push(Alert.HIDE_NSFW);
  }

  const results = await getFormattedObjects(
    query.map((item) => item.thread_id),
    userId,
    tags
  );

  results.sort((alertA: any, alertB: any) => {
    if (alertA.unreadPosts > alertB.unreadPosts) return -1;
    if (alertA.unreadPosts < alertB.unreadPosts) return 1;

    if (alertA.lastPost.createdAt > alertB.lastPost.createdAt) return -1;
    if (alertA.lastPost.createdAt < alertB.lastPost.createdAt) return 1;
    return 0;
  });

  return results;
};

export const getAlerts = async (userId: number, userUsergroup: number, page: number) => {
  const results = await getAlertsQuery(userId, userUsergroup);
  return results.slice((page - 1) * alertsPerPage, page * alertsPerPage);
};

export const getAlertsWithCount = async (
  userId: number,
  userUsergroup: number,
  page: number,
  hideNsfw: boolean = false
) => {
  const results = await getAlertsQuery(userId, userUsergroup, hideNsfw);
  // if no page parameter is provided, default to page 1
  if (!Number(page)) {
    return results.slice(0, alertsPerPage);
  }
  return {
    totalAlerts: results.length,
    alerts: results.slice((page - 1) * alertsPerPage, page * alertsPerPage),
  };
};

export const index = async (req: Request, res: Response) => {
  try {
    if (!req.isLoggedIn) {
      res.status(httpStatus.OK);
      res.json([]);
      return;
    }

    const hideNsfw = req.query.hideNsfw || false;
    const pageParam: number = req.params.page;

    const results = await getAlertsWithCount(req.user.id, req.user.usergroup, pageParam, hideNsfw);

    res.status(httpStatus.OK);
    res.json(results);
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};
