import { Request, Response } from 'express';
import httpStatus from 'http-status';
import knex from '../../services/knex';
import { reportsPerPage } from '../../constants/pagination';
import { getFormattedObjects } from '../../retriever/report';

const getResultCount = async (openFilter: boolean) => {
  const result = await knex('Reports')
    .count({ count: 'id' })
    .modify((queryBuilder) => {
      if (openFilter) {
        queryBuilder.where('solved_by', null);
      }
    })
    .first();

  return result.count;
};

const getResults = async (page: number, openFilter: boolean) => {
  const results = await knex('Reports')
    .pluck('id')
    .modify((queryBuilder) => {
      if (openFilter) {
        queryBuilder.where('solved_by', null);
      }
    })
    .orderBy('id', 'desc')
    .limit(reportsPerPage)
    .offset(page * reportsPerPage - reportsPerPage);

  const output = await getFormattedObjects(results);

  return output;
};

// eslint-disable-next-line import/prefer-default-export
export const get = async (req: Request, res: Response) => {
  const page = Number(req.params.page) || 1;
  const openFilter = req.query.open;
  const resultCount = await getResultCount(openFilter);
  const results = await getResults(page, openFilter);

  res.status(httpStatus.OK);
  res.json({
    totalReports: resultCount,
    currentPage: page,
    reports: results,
  });
};
