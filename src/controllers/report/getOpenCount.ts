import { Request, Response } from 'express';
import httpStatus from 'http-status';
import knex from '../../services/knex';
import moderatorOnly from '../../helpers/moderatorOnly';

export const get = async (req: Request, res: Response) => {

  moderatorOnly(req, res);

  const result = await knex('Reports')
    .count({ count: 'id' })
    .whereNull('solved_by')
    .first();

  res.status(httpStatus.OK);
  res.json({ openReports: result.count });

};