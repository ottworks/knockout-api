import { Request, Response } from 'express';
import httpStatus from 'http-status';
import { invalidateObject } from '../../retriever/report';
import errorHandler from '../../services/errorHandler';
import knex from '../../services/knex';

// eslint-disable-next-line import/prefer-default-export
export const post = async (req: Request, res: Response) => {
  try {
    await knex('Reports')
      .where({ id: req.params.id })
      .update({ solved_by: req.user.id, updated_at: knex.fn.now() });

    invalidateObject(req.params.id);
    res.status(httpStatus.CREATED);
    res.json({ message: 'Report marked as solved.' });
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};
