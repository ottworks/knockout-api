import knex from '../services/knex';
import { POST_CHARACTER_LIMIT, NEWLINE_MULTIPLIER } from '../constants/limits';

/**
 * @returns validation passed?
 */
const validatePostLength = (content) => {
  const postContent = content.trim();
  if (!postContent.length) {
    return false;
  }

  const lineBreakAdjustment = (postContent.match(/\n/g) || '').length * NEWLINE_MULTIPLIER;
  if (postContent.length + lineBreakAdjustment > POST_CHARACTER_LIMIT) {
    return false;
  }
  if (postContent.length < 1) {
    return false;
  }

  return true;
};

/**
 * async function (requires await) that returns a bool if a thread can be posted on
 * @returns validation passed?
 */
const validateThreadStatus = async (threadId: number) => {
  if (!threadId) {
    return false;
  }

  try {
    const targetThread = await knex
      .select('locked', 'deleted_at as deletedAt')
      .from('Threads')
      .where({ id: threadId });

    if (!targetThread[0] || targetThread[0].locked === 1 || targetThread[0].deletedAt) {
      return false;
    }
    return true;
  } catch (error) {
    console.log(error);

    return false;
  }
};

const VALID_APPS = ['knockout.chat', 'lite.knockout.chat', 'kopunch', 'knocky'];

const validateAppName = (clientName: string) => {
  if (VALID_APPS.includes(clientName)) {
    return true;
  }

  return false;
};

export { validatePostLength, validateThreadStatus, validateAppName };
