import { User } from 'express';

import { getFormattedObject } from '../retriever/user';

const LIMITED_USER_POST_COUNT = 15;

const validateUserFields = (user: User) => {
  if (!user) {
    return false;
  }
  if (!user.username || !user.id || !user.avatar_url || !user.usergroup) {
    console.error('user validation failed.');
    console.error('user.username', user.username);
    console.error('user.id', user.id);
    console.error('user.avatar_url', user.avatar_url);
    console.error('user.usergroup', user.usergroup);
    return false;
  }
  if (user.username === '') {
    console.error('user validation failed.');
    console.error('user.username', user.username);
    return false;
  }
  if (user.username.length < 3) {
    console.error('user validation failed.');
    console.error('user.username is ', user.username);
    return false;
  }
  return true;
};

const isLimitedUser = async (userId: number) => {
  // users under a week old should be considered suspicious under certain conditions
  // this validator is used in conjunction with a VPN/datacenter IP check to prevent alts
  const user = await getFormattedObject(userId);

  if (!user) return true;

  const now = new Date();
  const cutoff = new Date(user.createdAt);
  cutoff.setDate(cutoff.getDate() + 7);
  return now.getTime() < cutoff.getTime() || user.posts < LIMITED_USER_POST_COUNT;
};

export { validateUserFields, isLimitedUser };
