const { ShortcodeTree } = require('shortcode-tree');

const MENTION_REGEX = /@<(\d+)>/g;

const postMentionFinder = (content: string): number[] | false => {
  const mentions = [];
  try {
    // handle deprecated `mentionsUser` property on quotes
    const tree = ShortcodeTree.parse(content);
    tree.children.forEach((node) => {
      if (
        node.shortcode &&
        node.shortcode.name === 'quote' &&
        node.shortcode.properties.mentionsUser
      ) {
        mentions.push(Number(node.shortcode.properties.mentionsUser));
      }
    });

    // handle mentions
    let match = MENTION_REGEX.exec(content);

    // using matchAll here requires us to
    // transpile es2020, going to use a
    // less elegant solution
    while (match && match[0] !== '') {
      mentions.push(match[1]);
      match = MENTION_REGEX.exec(content);
    }

    return mentions;
  } catch (error) {
    console.log(error);
    return false;
  }
};

export default postMentionFinder;
