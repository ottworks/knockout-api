import { NextFunction, Request, Response } from "express";
import httpStatus from "http-status";
import { MODERATOR_GROUPS } from '../constants/userGroups';

const moderatorOnly = (req: Request, res: Response) => {
  if (!req.isLoggedIn || !req.user || !MODERATOR_GROUPS.includes(req.user.usergroup) || req.user.isBanned) {
    res.status(httpStatus.FORBIDDEN);
    res.json({ error: "Forbidden" });
    throw new Error("Not authorized.");
  }
};


export const moderatorOnlyMiddleware = (req: Request, res: Response, next: NextFunction) => {
  if (!req.isLoggedIn || !req.user || !MODERATOR_GROUPS.includes(req.user.usergroup) || req.user.isBanned) {
    res.status(httpStatus.FORBIDDEN).send({ error: "Forbidden" });
    return;
  }
  next();
};

export default moderatorOnly;
