import { Request } from "express";

const roundNumber = (num: number) => Math.ceil(num);
const createRedirectUrl = (req: Request, uri?: string, forcedProtocolString?: string) => `${forcedProtocolString || req.protocol}://${req.get('host')}${uri || ''}`;

export {
  roundNumber,
  createRedirectUrl
};