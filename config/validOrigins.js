const validOrigins = [
  'https://knockout.chat',
  'https://prod.knockout.chat',
  'https://qa.knockout.chat',
  'https://forums.stylepunch.club',
  'http://localhost:8080',
  'http://lite.knockout.local',
  'http://lite.knockout.chat',
  'https://lite.knockout.chat',
  'https://kopunch.club'
];

export default validOrigins;